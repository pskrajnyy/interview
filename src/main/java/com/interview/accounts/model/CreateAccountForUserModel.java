package com.interview.accounts.model;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CreateAccountForUserModel {
    private String name;
    private String lastName;
    private String pesel;
    private BigDecimal initialValueInPln;
}
