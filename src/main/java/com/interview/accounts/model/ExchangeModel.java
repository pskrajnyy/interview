package com.interview.accounts.model;

import com.interview.accounts.enums.Currency;
import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ExchangeModel {
    private BigDecimal amount;
    private Currency from;
    private Currency to;
    private String pesel;
}
