package com.interview.accounts.repository;

import com.interview.accounts.domain.subaccount.Subaccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface SubaccountRepository extends JpaRepository<Subaccount, Long>, JpaSpecificationExecutor<Subaccount> {
}
