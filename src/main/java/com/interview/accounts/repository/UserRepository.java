package com.interview.accounts.repository;

import com.interview.accounts.domain.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

    User findByNameAndLastNameAndPesel(String name, String lastName, String pesel);

    User findByPesel(String pesel);
}
