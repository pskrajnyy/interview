package com.interview.accounts.utils;

import com.interview.accounts.domain.subaccount.Subaccount;
import com.interview.accounts.domain.user.User;
import com.interview.accounts.enums.Currency;
import com.interview.accounts.exception.InvalidBodyInputException;
import com.interview.accounts.exception.RecordNotFoundException;
import com.interview.accounts.model.CreateAccountForUserModel;
import com.interview.accounts.model.ExchangeModel;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

@Service
public class AccountUtils {

    public void validateInputBody(CreateAccountForUserModel createAccountForUserModel) {
        if (createAccountForUserModel.getName() == null || createAccountForUserModel.getName().isEmpty()) {
            throw new InvalidBodyInputException("Please fill name field");
        }

        if (createAccountForUserModel.getLastName() == null || createAccountForUserModel.getLastName().isEmpty()) {
            throw new InvalidBodyInputException("Please fill lastName field");
        }

        if (createAccountForUserModel.getPesel() == null || createAccountForUserModel.getPesel().isEmpty()) {
            throw new InvalidBodyInputException("Please fill pesel field");
        }

        if (createAccountForUserModel.getInitialValueInPln() == null) {
            throw new InvalidBodyInputException("Please fill initialValueInPln field");
        }
    }

    public void validateExchangeBody(ExchangeModel exchangeModel) {
        if (exchangeModel.getPesel() == null || exchangeModel.getPesel().isEmpty()) {
            throw new InvalidBodyInputException("Please fill pesel field");
        }

        if (exchangeModel.getAmount() == null) {
            throw new InvalidBodyInputException("Please fill amount field");
        }

        if (exchangeModel.getFrom() == null) {
            throw new InvalidBodyInputException("Please fill from field");
        }

        if (exchangeModel.getTo() == null) {
            throw new InvalidBodyInputException("Please fill to field");
        }

        if (exchangeModel.getTo().equals(exchangeModel.getFrom())) {
            throw new InvalidBodyInputException("Uou must enter different currencies");
        }
    }

    public void checkThatUserHasFounds(User user, Currency currency, BigDecimal amount) {
        Optional<Subaccount> subaccount = user.getSubaccounts().stream().filter(e -> e.getCurrency().equals(currency)).findFirst();
        if (subaccount.isPresent()) {
            BigDecimal foundsOnAccount = subaccount.get().getAmount();
            if (foundsOnAccount.compareTo(amount) < 0) {
                throw new RecordNotFoundException("Brak srodkow na koncie");
            }
        }

    }

    public void currencyExchange(User userByPesel, Currency fromCurrency, Currency toCurrency, BigDecimal exchangeRate, BigDecimal amount) {
        Optional<Subaccount> subaccountFrom = userByPesel.getSubaccounts().stream().filter(e -> e.getCurrency().equals(fromCurrency)).findFirst();
        Optional<Subaccount> subaccountTo = userByPesel.getSubaccounts().stream().filter(e -> e.getCurrency().equals(toCurrency)).findFirst();

        if (fromCurrency.equals(Currency.PLN)) {
            if (subaccountFrom.isPresent() && subaccountTo.isPresent()) {
                Subaccount to = subaccountTo.get();
                Subaccount from = subaccountFrom.get();

                from.setAmount(from.getAmount().subtract(amount));
                to.setAmount(to.getAmount().add(amount.divide(exchangeRate,2, RoundingMode.HALF_UP)));
            }
        } else {
            if (subaccountFrom.isPresent() && subaccountTo.isPresent()) {
                Subaccount to = subaccountTo.get();
                Subaccount from = subaccountFrom.get();

                from.setAmount(from.getAmount().subtract(amount));
                to.setAmount(to.getAmount().add(amount.multiply(exchangeRate)));
            }
        }
    }
}
