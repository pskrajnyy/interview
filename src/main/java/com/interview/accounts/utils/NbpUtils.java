package com.interview.accounts.utils;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;

@Service
public class NbpUtils {

    public BigDecimal getExchangeRate() {
        RestTemplate restTemplate = new RestTemplate();
        LinkedHashMap exchangeRateNbp = restTemplate.getForObject("http://api.nbp.pl/api/exchangerates/rates/a/USD/today?format=json", LinkedHashMap.class);
        ArrayList rates = (ArrayList) exchangeRateNbp.get("rates");
        LinkedHashMap rate = (LinkedHashMap) rates.get(0);
        return BigDecimal.valueOf((Double) rate.get("mid"));
    }
}
