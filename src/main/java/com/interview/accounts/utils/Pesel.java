package com.interview.accounts.utils;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.Period;

public class Pesel extends Number {
    private static final int[] weightTable = {9, 7, 3, 1, 9, 7, 3, 1, 9, 7};

    private LocalDate birthDate;
    private int age;

    public Pesel(String number) {
        super(number);
        this.birthDate = null;
        this.age = 0;
    }

    @Override
    public void validate() {
        int[] tab = new int[11];
        int checksum = 0;

        if (isGood()) {
            for (int i = 0; i < 11; i++) {
                tab[i] = Character.getNumericValue(super.getNumber().charAt(i));
            }

            for (int i = 0; i < 10; i++) checksum += (tab[i] * weightTable[i]);
            checksum = checksum % 10;
            super.setCorrect(checksum == tab[10]);

            try {
                this.birthDate = calculateBirthDate();
                this.age = LocalDate.now().getYear() - this.birthDate.getYear();
            } catch (DateTimeException e) {
                super.setCorrect(false);
            }
        }
        super.setValidated();
    }

    @Override public boolean isGood() {
        boolean peselGood = true;
        if (super.getNumber() == null) peselGood = false;
        else if (super.getNumber().length() != 11) peselGood = false;
        return peselGood;
    }

    private LocalDate calculateBirthDate() throws DateTimeException {
        int[] numberAsIntArray = new int[11];
        for (int i = 0; i < 11; i++) {
            numberAsIntArray[i] = Character.getNumericValue(super.getNumber().charAt(i));
        }

        int birthDay = 10 * numberAsIntArray[4] + numberAsIntArray[5];
        int birthYear = 10 * numberAsIntArray[0] + numberAsIntArray[1];
        int birthMonth = 10 * numberAsIntArray[2] + numberAsIntArray[3];
        if (birthDay > 31) super.setCorrect(false);

        if (birthMonth <= 12) birthYear += 1900;
        else if (birthMonth <= 32) {
            birthYear += 2000;
            birthMonth -= 20;
        } else if (birthMonth <= 52) {
            birthYear += 2100;
            birthMonth -= 40;
        } else if (birthMonth <= 72) {
            birthYear += 2200;
            birthMonth -= 60;
        } else if (birthMonth <= 92) {
            birthYear += 1800;
            birthMonth -= 80;
        } else super.setCorrect(false);

        return LocalDate.of(birthYear, birthMonth, birthDay);
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public int getAge() {
        return age;
    }

    public void validateAge() {
        LocalDate birthDateFromPesel = getBirthDate();
        if (Period.between(birthDateFromPesel, LocalDate.now()).getYears() < 18) {
            throw new RuntimeException("Invalid age");
        }
    }
}
