package com.interview.accounts.utils;

public abstract class Number {

    private String number;
    private boolean validated;
    private boolean correct;

    public Number(String number) {
        this.number = prepareNumber(number);
        this.validated = false;
        this.correct = false;
    }

    private String prepareNumber(String number) {
        number = number.trim();
        number = number.replaceAll("\\s+", "");
        number = number.toUpperCase();
        StringBuilder builder = new StringBuilder();
        for (char c : number.toCharArray()) {
            if (Character.isLetterOrDigit(c)) builder.append(c);
        }
        return builder.toString();
    }

    public abstract void validate();

    public abstract boolean isGood();

    public void setNumber(String number) {
        this.number = prepareNumber(number);
        this.validated = false;
        this.correct = false;
    }

    public final String getNumber() {
        return number;
    }

    protected void setValidated() {
        this.validated = true;
    }

    public boolean isValidated() {
        return validated;
    }

    protected void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public final boolean isCorrect() {
        return correct;
    }
}
