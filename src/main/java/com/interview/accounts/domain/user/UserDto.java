package com.interview.accounts.domain.user;

import com.interview.accounts.domain.subaccount.SubaccountDto;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class UserDto {
    private Long userId;
    private String name;
    private String lastName;
    private String pesel;
    private List<SubaccountDto> subaccounts = new ArrayList<>();
}
