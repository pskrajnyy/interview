package com.interview.accounts.domain.user;

import com.interview.accounts.domain.subaccount.Subaccount;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;
    private String name;
    private String lastName;
    private String pesel;
    @ToString.Exclude
    @OneToMany(mappedBy = "user")
    private List<Subaccount> subaccounts = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(userId, user.userId) && Objects.equals(name, user.name) && Objects.equals(lastName, user.lastName) && Objects.equals(pesel, user.pesel) && Objects.equals(subaccounts, user.subaccounts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, name, lastName, pesel, subaccounts);
    }
}
