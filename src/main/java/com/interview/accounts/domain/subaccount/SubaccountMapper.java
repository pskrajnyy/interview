package com.interview.accounts.domain.subaccount;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
public interface SubaccountMapper {

    SubaccountMapper INSTANCE = Mappers.getMapper(SubaccountMapper.class);

    SubaccountDto entityToDto(Subaccount subaccount);

    List<SubaccountDto> entitiesToDtos(List<Subaccount> subaccountList);
}
