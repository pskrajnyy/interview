package com.interview.accounts.domain.subaccount;

import com.interview.accounts.domain.user.User;
import com.interview.accounts.enums.Currency;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Subaccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long subaccountId;
    private Currency currency;
    private BigDecimal amount;
    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Subaccount that = (Subaccount) o;
        return Objects.equals(subaccountId, that.subaccountId) && currency == that.currency && Objects.equals(amount, that.amount) && Objects.equals(user, that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subaccountId, currency, amount, user);
    }
}
