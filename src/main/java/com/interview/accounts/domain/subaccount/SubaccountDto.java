package com.interview.accounts.domain.subaccount;

import com.interview.accounts.enums.Currency;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class SubaccountDto {
    private Long subaccountId;
    private Currency currency;
    private BigDecimal amount;
}
