package com.interview.accounts.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class InvalidBodyInputException extends RuntimeException {

    private static final long serialVersionUID = 5861310537366287163L;

    public InvalidBodyInputException(final String message) {
        super(message);
        log.error(message);
    }
}


