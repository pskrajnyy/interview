package com.interview.accounts.controller;

import com.interview.accounts.domain.user.UserDto;
import com.interview.accounts.model.CreateAccountForUserModel;
import com.interview.accounts.model.ExchangeModel;
import com.interview.accounts.service.AccountService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("account")
@Validated
public class AccountController {
    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping()
    public ResponseEntity<UserDto> getAccountByPesel(@RequestParam String pesel) {
        return ResponseEntity.ok(accountService.getAccountByPesel(pesel));
    }

    @PostMapping
    public ResponseEntity<UserDto> createAccountForUser(@RequestBody CreateAccountForUserModel createAccountForUserModel) {
        return ResponseEntity.ok(accountService.createAccount(createAccountForUserModel));
    }

    @PostMapping("exchange")
    public ResponseEntity<UserDto> exchange(@RequestBody ExchangeModel exchangeModel) {
        return ResponseEntity.ok(accountService.exchange(exchangeModel));
    }
}
