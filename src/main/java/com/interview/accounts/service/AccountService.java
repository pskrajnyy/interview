package com.interview.accounts.service;

import com.interview.accounts.domain.user.UserDto;
import com.interview.accounts.model.CreateAccountForUserModel;
import com.interview.accounts.model.ExchangeModel;

public interface AccountService {
    UserDto createAccount(final CreateAccountForUserModel createAccountForUserModel);

    UserDto getAccountByPesel(final String pesel);

    UserDto exchange(final ExchangeModel exchangeModel);
}
