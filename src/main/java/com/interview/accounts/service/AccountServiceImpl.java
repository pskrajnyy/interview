package com.interview.accounts.service;

import com.interview.accounts.domain.subaccount.Subaccount;
import com.interview.accounts.domain.user.User;
import com.interview.accounts.domain.user.UserDto;
import com.interview.accounts.domain.user.UserMapper;
import com.interview.accounts.enums.Currency;
import com.interview.accounts.exception.RecordNotFoundException;
import com.interview.accounts.model.CreateAccountForUserModel;
import com.interview.accounts.model.ExchangeModel;
import com.interview.accounts.repository.SubaccountRepository;
import com.interview.accounts.repository.UserRepository;
import com.interview.accounts.utils.AccountUtils;
import com.interview.accounts.utils.NbpUtils;
import com.interview.accounts.utils.Pesel;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Arrays;

@Service("accountService")
public class AccountServiceImpl implements AccountService {
    private final UserRepository userRepository;
    private final SubaccountRepository subaccountRepository;
    private final AccountUtils accountUtils;
    private final NbpUtils nbpUtils;

    public AccountServiceImpl(UserRepository userRepository, SubaccountRepository subaccountRepository, AccountUtils accountUtils, NbpUtils nbpUtils) {
        this.userRepository = userRepository;
        this.subaccountRepository = subaccountRepository;
        this.accountUtils = accountUtils;
        this.nbpUtils = nbpUtils;
    }

    @Override
    public UserDto createAccount(CreateAccountForUserModel createAccountForUserModel) {
        accountUtils.validateInputBody(createAccountForUserModel);
        Pesel pesel = new Pesel(createAccountForUserModel.getPesel());
        pesel.validate();
        pesel.validateAge();

        User userFromDb = userRepository.findByNameAndLastNameAndPesel(createAccountForUserModel.getName(), createAccountForUserModel.getLastName(), createAccountForUserModel.getPesel());

        if (userFromDb == null) {
            User user = User
                    .builder()
                    .name(createAccountForUserModel.getName())
                    .lastName(createAccountForUserModel.getLastName())
                    .pesel(createAccountForUserModel.getPesel())
                    .build();
            createSubaccounts(user, createAccountForUserModel.getInitialValueInPln());


            userRepository.save(user);
            subaccountRepository.saveAll(user.getSubaccounts());
            return UserMapper.INSTANCE.entityToDto(user);
        }
        return UserMapper.INSTANCE.entityToDto(userFromDb);
    }

    @Override
    public UserDto getAccountByPesel(String pesel) {
        User userByPesel = userRepository.findByPesel(pesel);
        if (userByPesel == null) {
            throw new RecordNotFoundException("User with pesel: " + pesel + " not exists");
        }
        return UserMapper.INSTANCE.entityToDto(userByPesel);
    }

    @Override
    public UserDto exchange(ExchangeModel exchangeModel) {
        accountUtils.validateExchangeBody(exchangeModel);
        User userByPesel = userRepository.findByPesel(exchangeModel.getPesel());
        if (userByPesel == null) {
            throw new RecordNotFoundException("User with pesel: " + exchangeModel.getPesel() + " not exists");
        }
        BigDecimal exchangeRate = nbpUtils.getExchangeRate();

        if (exchangeModel.getFrom().equals(Currency.PLN)) {
            accountUtils.checkThatUserHasFounds(userByPesel, Currency.PLN, exchangeModel.getAmount());
            accountUtils.currencyExchange(userByPesel, Currency.PLN, Currency.USD, exchangeRate, exchangeModel.getAmount());
        } else if (exchangeModel.getFrom().equals(Currency.USD)) {
            accountUtils.checkThatUserHasFounds(userByPesel, Currency.USD, exchangeModel.getAmount());
            accountUtils.currencyExchange(userByPesel, Currency.USD, Currency.PLN, exchangeRate, exchangeModel.getAmount());
        }

        userRepository.save(userByPesel);
        return UserMapper.INSTANCE.entityToDto(userByPesel);
    }

    private void createSubaccounts(User user, BigDecimal initialAmountInPln) {
        Subaccount subaccountPln = Subaccount
                .builder()
                .user(user)
                .amount(initialAmountInPln)
                .currency(Currency.PLN)
                .build();

        Subaccount subaccountUsd = Subaccount
                .builder()
                .user(user)
                .amount(BigDecimal.ZERO)
                .currency(Currency.USD)
                .build();
        user.setSubaccounts(Arrays.asList(subaccountPln, subaccountUsd));
    }
}
