# POSTMAN COLLECTION

{
"info": {
"_postman_id": "d0ae1bb0-4f38-4de7-9442-16011bc97cc9",
"name": "New Collection",
"schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json",
"_exporter_id": "8621382"
},
"item": [
{
"name": "CreateAccount",
"request": {
"method": "POST",
"header": [],
"body": {
"mode": "raw",
"raw": "{\n    \"name\": \"Przemek\",\n    \"lastName\": \"Skrajny\",\n    \"pesel\": \"94101507539\",\n    \"initialValueInPln\": 10.98\n}",
"options": {
"raw": {
"language": "json"
}
}
},
"url": {
"raw": "localhost:8080/account",
"host": [
"localhost"
],
"port": "8080",
"path": [
"account"
]
}
},
"response": []
},
{
"name": "getAccountByPesel",
"request": {
"method": "GET",
"header": [],
"url": {
"raw": "localhost:8080/account?pesel=94101507539",
"host": [
"localhost"
],
"port": "8080",
"path": [
"account"
],
"query": [
{
"key": "pesel",
"value": "94101507539"
}
]
}
},
"response": []
},
{
"name": "Exchange",
"request": {
"method": "POST",
"header": [],
"body": {
"mode": "raw",
"raw": "{\n    \"amount\": \"10\",\n    \"from\": \"PLN\",\n    \"to\": \"USD\",\n    \"pesel\": \"94101507539\"\n}",
"options": {
"raw": {
"language": "json"
}
}
},
"url": {
"raw": "localhost:8080/account/exchange",
"host": [
"localhost"
],
"port": "8080",
"path": [
"account",
"exchange"
]
}
},
"response": []
}
]
}
